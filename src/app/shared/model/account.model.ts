export interface Account {
  id: number;
  username: string;
  password: string;
  name: string;
  genderID: number;
  gender: string;
  roleID: number;
  role: string;
  phone: string;
  email: string;
}
