interface ProductSize {
  id: number;
  name: string;
  price: number;
}

export interface Product {
  id: number;
  name: string;
  description: string;
  imageURL: string;
  categoryID: number;
  category: string;
  sizes: Array<ProductSize>;
  _size: string; // combine sizes together: S M, S M L, M L
}
