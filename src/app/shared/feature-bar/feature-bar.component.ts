import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-feature-bar',
  templateUrl: './feature-bar.component.html',
  styleUrls: ['./feature-bar.component.css'],
})
export class FeatureBarComponent implements OnInit {
  @Input()
  title: string = '';

  @Output()
  onButtonAddClick = new EventEmitter<void>();

  @Output()
  onSearchTextChange = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {
    this.title = this.title.toUpperCase();
  }

  addButtonClicked() {
    this.onButtonAddClick.emit();
  }

  searchTextChanged(event: Event) {
    const input = event.target as HTMLInputElement;
    this.onSearchTextChange.emit(input.value);
  }
}
