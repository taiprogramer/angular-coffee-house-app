import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  @Input() urls: Array<string> = [];
  @Input() icons: Array<string> = [];
  @Input() activeUrl: string = '';

  @Output()
  onActiveUrlChange = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}
  activeUrlChanged(url: string) {
    this.onActiveUrlChange.emit(url);
  }
}
