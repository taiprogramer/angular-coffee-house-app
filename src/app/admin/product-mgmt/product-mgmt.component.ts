import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ProductService } from 'src/app/product.service';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { Product } from 'src/app/shared/model/product.model';
import { ProductDialogComponent } from './product-dialog/product-dialog.component';

@Component({
  selector: 'app-product-mgmt',
  templateUrl: './product-mgmt.component.html',
  styleUrls: ['./product-mgmt.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ProductMgmtComponent implements OnInit {
  displayedColumns = ['name', 'imageURL', 'category', '_size', 'action'];
  pageSizeOptions = [2, 5, 10];
  dataSource = new MatTableDataSource<Product>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private productService: ProductService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts().subscribe((response) => {
      const products = response.data.map((product) => {
        product._size = product.sizes.map((size) => size.name).join(' ');
        return product;
      });
      this.dataSource = new MatTableDataSource<Product>(products);
      this.dataSource.paginator = this.paginator;
    });
  }

  searchBySearchText(text: string) {
    this.productService.getProducts().subscribe((response) => {
      const products = response.data.map((product) => {
        product._size = product.sizes.map((size) => size.name).join(' ');
        return product;
      });
      text = text.toLowerCase();
      const filteredProducts = products.filter((product) => {
        const validSearch =
          product.name.toLowerCase().includes(text) ||
          product.category.toLowerCase().includes(text) ||
          product._size.toLowerCase().includes(text);
        return validSearch;
      });
      this.dataSource = new MatTableDataSource<Product>(filteredProducts);
      this.dataSource.paginator = this.paginator;
    });
  }

  openDialog(
    title: string,
    product: Product,
    onConfirm: (product: Product) => void
  ): void {
    const dialogRef = this.dialog.open(ProductDialogComponent, {
      width: '450px',
      data: {
        title,
        product,
        onConfirm,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {});
  }

  createEmptyProduct(): Product {
    const product: Product = {
      id: 0,
      name: '',
      imageURL: '',
      categoryID: 0,
      category: '',
      sizes: [],
      _size: '',
      description: '',
    };
    return product;
  }

  openAddProductDialog(): void {
    const product = this.createEmptyProduct();
    this.openDialog('Create new product', product, (product: Product) => {
      this.addProduct(product);
    });
  }

  addProduct(product: Product) {
    this.productService
      .addProduct(product)
      .subscribe((_) => this.getProducts());
  }

  updateProduct(product: Product) {
    this.productService
      .updateProduct(product)
      .subscribe((_) => this.getProducts());
  }

  deleteProduct(product: Product) {
    this.productService.deleteProduct(product).subscribe((_) => {
      this.getProducts();
    });
  }

  openDeleteConfirmDialog(product: Product) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '450px',
      data: {
        title: 'Are you sure?',
        body: `${product.name} will be deleted after you click confirm.`,
        onConfirm: () => {
          this.deleteProduct(product);
          dialogRef.close();
        },
      },
    });
  }

  openEditProductDialog(product: Product) {
    this.openDialog('Update product', product, (product: Product) => {
      this.updateProduct(product);
    });
  }
}
