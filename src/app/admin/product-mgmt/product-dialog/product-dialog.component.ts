import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from 'src/app/shared/model/product.model';

export interface ProductDialogData {
  title: string;
  product: Product;
  onConfirm: (product: Product) => void;
}

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.css'],
})
export class ProductDialogComponent implements OnInit {
  productForm = new FormGroup({});
  constructor(
    public dialogRef: MatDialogRef<ProductDialogData>,
    @Inject(MAT_DIALOG_DATA) public data: ProductDialogData
  ) {}

  ngOnInit(): void {
    this.productForm = new FormGroup({
      id: new FormControl(this.data.product.id),
      name: new FormControl(this.data.product.name),
      categoryID: new FormControl(this.data.product.categoryID.toString()),
      description: new FormControl(this.data.product.description),
      sizes: new FormControl(this.data.product.sizes),
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    const product: Product = this.productForm.value;
    this.data.onConfirm(product);
    this.dialogRef.close();
  }
}
