import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductMgmtComponent } from './product-mgmt.component';

const routes: Routes = [{ path: '', component: ProductMgmtComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductMgmtRoutingModule { }
