import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
})
export class AdminComponent implements OnInit {
  icons = ['group', 'free_breakfast'];
  urls = ['/admin/account-mgmt', '/admin/product-mgmt'];
  activeUrl: string = '';
  constructor(private router: Router) {}

  ngOnInit(): void {
    this.activeUrl = this.router.url;
  }

  changeActiveUrl(url: string) {
    this.activeUrl = url;
  }
}
