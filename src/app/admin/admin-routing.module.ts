import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'account-mgmt',
        loadChildren: () =>
          import('./account-mgmt/account-mgmt.module').then(
            (m) => m.AccountMgmtModule
          ),
      },
      {
        path: 'product-mgmt',
        loadChildren: () =>
          import('./product-mgmt/product-mgmt.module').then(
            (m) => m.ProductMgmtModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
