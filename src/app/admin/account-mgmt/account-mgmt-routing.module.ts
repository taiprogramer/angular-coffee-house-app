import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountMgmtComponent } from './account-mgmt.component';

const routes: Routes = [{ path: '', component: AccountMgmtComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountMgmtRoutingModule { }
