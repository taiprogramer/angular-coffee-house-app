import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { AccountService } from 'src/app/account.service';
import { Account } from 'src/app/shared/model/account.model';
import { MatDialog } from '@angular/material/dialog';
import { AccountDialogComponent } from './account-dialog/account-dialog.component';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-account-mgmt',
  templateUrl: './account-mgmt.component.html',
  styleUrls: ['./account-mgmt.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AccountMgmtComponent implements OnInit {
  displayedColumns = ['name', 'gender', 'role', 'phone', 'email', 'action'];
  pageSizeOptions = [2, 5, 10];
  dataSource = new MatTableDataSource<Account>([]);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private accountService: AccountService,
    public dialog: MatDialog
  ) {}

  openDialog(title: string, account?: Account): void {
    const dialogRef = this.dialog.open(AccountDialogComponent, {
      width: '450px',
      data: {
        title,
        account,
        onConfirm: (account: Account) => {
          this.editAccount(account);
        },
      },
    });
    dialogRef.afterClosed().subscribe((result) => {});
  }

  openEditAccountDialog(account: Account): void {
    this.openDialog('Update account', account);
  }

  openAddAccountDialog(): void {
    const account: Account = {
      id: 0,
      username: '',
      password: '',
      name: '',
      genderID: 0,
      gender: '',
      roleID: 0,
      role: '',
      phone: '',
      email: '',
    };
    this.openDialog('Create new account', account);
  }

  ngOnInit(): void {
    this.getAccounts();
  }

  getAccounts() {
    this.accountService.getAccounts().subscribe((accounts) => {
      this.dataSource = new MatTableDataSource<Account>(accounts);
      this.dataSource.paginator = this.paginator;
    });
  }

  editAccount(account: Account) {
    this.accountService
      .editAccount(account)
      .subscribe((_) => this.getAccounts());
  }

  addAccount(account: Account) {
    this.accountService
      .addAccount(account)
      .subscribe((_) => this.getAccounts());
  }

  deleteAccount(account: Account) {
    this.accountService
      .deleteAccount(account)
      .subscribe((_) => this.getAccounts());
  }

  openDeleteConfirmDialog(account: Account) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '450px',
      data: {
        title: 'Are you sure?',
        body: `${account.name} will be deleted after you click confirm.`,
        onConfirm: () => {
          this.deleteAccount(account);
          dialogRef.close();
        },
      },
    });
    dialogRef.afterClosed().subscribe((result) => {});
  }

  searchBySearchText(text: string) {
    this.accountService.getAccounts().subscribe((accounts) => {
      text = text.toLowerCase();
      const filteredAccounts = accounts.filter((account) => {
        const validSearch =
          account.name.toLowerCase().includes(text) ||
          account.email.toLowerCase().includes(text) ||
          account.username.toLowerCase().includes(text) ||
          account.phone.toLowerCase().includes(text);
        return validSearch;
      });
      this.dataSource = new MatTableDataSource<Account>(filteredAccounts);
      this.dataSource.paginator = this.paginator;
    });
  }
}
