import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountMgmtRoutingModule } from './account-mgmt-routing.module';
import { AccountMgmtComponent } from './account-mgmt.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AccountDialogComponent } from './account-dialog/account-dialog.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  declarations: [AccountMgmtComponent, AccountDialogComponent],
  imports: [
    CommonModule,
    AccountMgmtRoutingModule,
    SharedModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatSelectModule,
  ],
})
export class AccountMgmtModule {}
