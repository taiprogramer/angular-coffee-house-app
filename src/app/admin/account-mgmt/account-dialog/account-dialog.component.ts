import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Account } from 'src/app/shared/model/account.model';

export interface AccountDialogData {
  title: string;
  account: Account;
  onConfirm: (account: Account) => void;
}

@Component({
  selector: 'app-account-dialog',
  templateUrl: './account-dialog.component.html',
  styleUrls: ['./account-dialog.component.css'],
})
export class AccountDialogComponent implements OnInit {
  accountForm = new FormGroup({});

  constructor(
    public dialogRef: MatDialogRef<AccountDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AccountDialogData
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
    this.accountForm = new FormGroup({
      id: new FormControl(this.data.account.id),
      username: new FormControl(this.data.account.username),
      password: new FormControl(''),
      email: new FormControl(this.data.account.email),
      name: new FormControl(this.data.account.name),
      genderID: new FormControl(this.data.account.genderID.toString()),
      roleID: new FormControl(this.data.account.roleID.toString()),
      phone: new FormControl(this.data.account.phone),
    });
  }

  onConfirm() {
    const account: Account = this.accountForm.value;
    this.data.onConfirm(account);
    this.dialogRef.close();
  }
}
