import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from './shared/model/product.model';

interface GeneralResponse<T> {
  data: T;
  error: any;
}

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private baseUrl = 'http://localhost:8080/product';

  constructor(private http: HttpClient) {}

  getProducts(): Observable<GeneralResponse<Product[]>> {
    return this.http.get<GeneralResponse<Product[]>>(this.baseUrl);
  }

  addProduct(product: Product): Observable<GeneralResponse<Product>> {
    return this.http.post<GeneralResponse<Product>>(this.baseUrl, product);
  }

  deleteProduct(product: Product): Observable<GeneralResponse<string>> {
    return this.http.delete<GeneralResponse<string>>(
      `${this.baseUrl}/${product.id}`
    );
  }

  updateProduct(product: Product): Observable<GeneralResponse<Product>> {
    return this.http.put<GeneralResponse<Product>>(
      `${this.baseUrl}/${product.id}`,
      product
    );
  }
}
