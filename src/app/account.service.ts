import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Account } from './shared/model/account.model';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  private baseUrl = 'http://localhost:8080/user';

  constructor(private http: HttpClient) {}

  getAccounts(): Observable<Account[]> {
    // right now, offset and limit are not working,
    // maybe they will work in the future
    return this.http.get<Account[]>(`${this.baseUrl}?offset=1&limit=1`);
  }

  editAccount(account: Account): Observable<Account> {
    return this.http.put<Account>(`${this.baseUrl}/${account.id}`, account);
  }

  addAccount(account: Account): Observable<Account> {
    return this.http.post<Account>(`${this.baseUrl}`, account);
  }

  deleteAccount(account: Account): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${account.id}`);
  }
}
